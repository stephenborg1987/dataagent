package com.qb.producers

import java.util

import com.google.gson.Gson
import com.qb.consumers.helpers.models.FileScan
import org.apache.http._
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpPost
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.message.BasicNameValuePair

/**
  * Created by Stephen Borg on 10/10/2016.
  */
class FileSender {

  /**
    * Used to send data to a service
    */
   def sendData(restURL : String, fileScan : FileScan): Boolean = {

    try {
      //Specific headers...
      val post = new HttpPost(restURL)
      post.addHeader("Authorisation", "")
      post.addHeader("fileType", fileScan.FileType)

      val client = new DefaultHttpClient

      val nameValuePairs = new util.ArrayList[NameValuePair](1)
      nameValuePairs.add(new BasicNameValuePair("userKey", ""));
      nameValuePairs.add(new BasicNameValuePair("userPassword", ""));

      val gson = new Gson
      val jsonString = gson.toJson(fileScan)

      nameValuePairs.add(new BasicNameValuePair("fileData", jsonString));

      post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

      // send the post request
      val response = client.execute(post)
      response.getAllHeaders.foreach(arg => println(arg))

      // successful
      true

    } catch {
      case e: Exception => {
        // For now...
        e.printStackTrace()

        // Return unsuccessful
        false
      }
    }
  }
}
