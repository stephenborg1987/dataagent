package com.qb.consumers.helpers

import akka.actor.{ActorRef, ActorSystem, Props}
import com.qb.consumers.dataAgent.{Configuration, FileConsumer}
import com.qb.consumers.helpers.actors.DataAgentManager
import com.qb.utilities.ConfigurationUtils

/**
  * Created by Stephen Borg on 02/10/2016.
  */
class AgentBoot extends  App
{
  // Initialise the properties, and load them
  val config = new Configuration {
    override val config = ConfigurationUtils(args)
  }

  // Initialise akka system for our data agent
  implicit val system = ActorSystem("qb-dataAgent")

  val cacheManager: ActorRef = system.actorOf(Props(classOf[DataAgentManager], Map()), "qb-manager")
  val agentBooter = new FileConsumer(config.consumerConfig, cacheManager)
  agentBooter.run()
}
