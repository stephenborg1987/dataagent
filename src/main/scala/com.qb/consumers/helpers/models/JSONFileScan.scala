package com.qb.consumers.helpers.models

import java.nio.file.Path
import java.util.Date


/**
  * Created by Stephen Borg on 02/10/2016.
  */
case class JSONFileScan(FileName: String, DateProcessed: Date, Content: String, FileType : String, FilePath : Path)  extends JSONFileValidator with FileScan { validate(this.Content)

  override def Validate(content : String): Boolean = {
    validate(content)
  }
}

sealed trait JSONFileValidator {
  def validate(content: String): Boolean =
  {
    true
  }
}