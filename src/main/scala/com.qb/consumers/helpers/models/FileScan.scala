package com.qb.consumers.helpers.models

import java.nio.file.Path
import java.util.Date


/**
  * Created by Stephen Borg on 08/10/2016.
  */
trait  FileScan {
  def FileName: String
  def DateProcessed: Date
  def Content: String
  def FileType: String
  def FilePath :Path
  def Validate(content : String) : Boolean
}

