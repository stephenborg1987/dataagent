package com.qb.consumers.helpers.actors

import com.qb.consumers.helpers.actors.DataAgentManager.{Push, DataEntry, DoNothing}
import com.qb.consumers.helpers.models.FileScan

/**
  * Created by Stephen Borg on 02/10/2016.
  */
object DataAgentManager {
  case class DataEntry(file: FileScan)

  // Remove and store file in incorrect file repository
  object Bin

  // Do absolutely nothing...
  object DoNothing

  // Push to REST API
  object Push
}

/**
  * So we are going to have a map of the file name, which should be unique. We are going to force the client to
  * store the files in the form of yyyyMMddhhssfff.[csv/json/xml]
  */
case class DataAgentContext(fileScanMap: Map[String, String], lastFileScan: Option[FileScan])

/**
  * Via akka we are going to pass the maps having a list of files since we are always polling.
  * The file types are going to be the list of extensions supported for this data agent.
  * */
class DataAgentManager(initialMap: Map[String, String],fileTypes : Array[String]) extends InstrumentedActor
{
  /** Actor classes should implement this partialFunction for standard actor message handling */
  override def wrappedReceive: Receive = coordinate(DataAgentContext(initialMap, None))

  def coordinate(fileScanContext: DataAgentContext): Receive = {
    case DataEntry(file) =>
      logger.info("File map - " + fileScanContext.fileScanMap)

      fileScanContext.fileScanMap.get(file.FileName) match {
        case None =>
          val newMap = fileScanContext.fileScanMap + ((file.FileName, "Placed"))
          sender ! DoNothing
          context.become(coordinate(DataAgentContext(newMap, Some(file))))
        case Some(c) =>
          sender ! Push
          context.become(coordinate(DataAgentContext(fileScanContext.fileScanMap.updated(file.FileName,file.Content), Some(file))))
      }
  }
}
