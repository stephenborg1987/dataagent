package com.qb.consumers.helpers.actors

import akka.actor.Actor
import org.slf4j.LoggerFactory

/**
  * Created by Stephen Borg on 02/10/2016.
  */
trait ActorStack extends Actor
{
  /** Actor classes should implement this partialFunction for standard actor message handling */
  def wrappedReceive: Receive

  /** Stackable traits should override and call super.receive(x) for stacking functionality */
  def receive: Receive = {
    case x => if (wrappedReceive.isDefinedAt(x)) wrappedReceive(x) else unhandled(x)
  }
}

trait ActorLogging extends ActorStack {
  val logger = LoggerFactory.getLogger(getClass)
  private[this] val myPath = self.path.toString

  withAkkaSourceLogging {
    logger.info("Starting actor " + getClass.getName)
  }

  override def receive: Receive = {
    case x =>
      withAkkaSourceLogging {
        super.receive(x)
      }
  }

  private def withAkkaSourceLogging(fn: => Unit) {
    // Because each actor receive invocation could happen in a different thread, and MDC is thread-based,
    // we kind of have to set the MDC anew for each receive invocation.  :(
    try {
      org.slf4j.MDC.put("akkaSource", myPath)
      fn
    } finally {
      org.slf4j.MDC.remove("akkaSource")
    }
  }
}
