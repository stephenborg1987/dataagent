package com.qb.consumers.dataAgent

import java.io.File
import java.nio.file.{Files, Path, Paths, StandardCopyOption}
import java.util.Date
import java.util.concurrent._

import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import com.qb.consumers.helpers.actors.DataAgentManager.DataEntry
import com.qb.producers.FileSender
import com.qb.utilities.FileUtilities
import com.sun.javafx.util.Logging

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by Stephen Borg on 02/10/2016.
  * Based on http://doc.akka.io/docs/akka/snapshot/scala/actors.html
  */
class FileConsumer(consumerConfiguration: ConsumerConfiguration, dataAgentManager: ActorRef) extends Logging with Runnable {
  implicit val timeout = new Timeout(consumerConfiguration.akkaProcessTimeout)

  var executor: ExecutorService = _

  def getRequiredProperties(filePath: Path): (String, String, String, Path) = {

    // Get the filename, and split into an array with filename, and also extension
    var fileInformation = filePath.getFileName.toString.split("\\.(?=[^\\.]+$)")

    // We need to have the file formatted properly
    if ((fileInformation.length<=1) || (fileInformation(0).equals("") || fileInformation(1).equals(""))) {
      null
      //throw new IndexOutOfBoundsException("Incorrect format of file name! File may possibly have an extension missing!")
    } else {

      // Pass this back - Name/Extension/FileContent
      (fileInformation(0), fileInformation(1), scala.io.Source.fromFile(filePath.toString, consumerConfiguration.fileFormat).mkString, filePath)
    }
  }

  /**
    * This is the implementation of the consumer, that accepts certain types of files depending on the configuration. We are returning a
    * tuple that has the file name, and extension, so we can decide to filter on the easily. The only properties that we are concerned of
    * are filename/extension. Keeping the agent processing limited properties.
    **/
  override def run(): Unit = {

    // Function to scan the folder
    def scanFolder() = {

      // Stream directory, and map a list of filename, and extension
      val files = Files.newDirectoryStream(Paths.get(consumerConfiguration.directory)).map((data: (Path)) => getRequiredProperties(data))

      // Filter on those that are allowed for the data agent! In this case it's CSV...
      files.filter(x => x != null && consumerConfiguration.fileTypes.contains(x._2)).foreach(f = x => {

        // Prepare object to pass
        val file = new FileUtilities()

        // Get file depending on extension
        val fileScan = file.getFile(x._1, new Date(), x._3, x._2, x._4)

        // All file types are implementing a different validator depending if JSON, CSV and so on...
        dataAgentManager.ask(DataEntry(fileScan)).onSuccess {

          case (_) =>
            var fileSender = new FileSender()
            var result = fileSender.sendData(consumerConfiguration.serviceURL,fileScan)

            val currentFile = new File(fileScan.FilePath.toString).toPath
            val newFile = new File(consumerConfiguration.processedRepository + "/" + fileScan.FileName + "." + fileScan.FileType).toPath
            Files.move(currentFile, newFile, StandardCopyOption.ATOMIC_MOVE)

            Files.deleteIfExists(fileScan.FilePath)
        }
      })


    }

    // Convert our configuration to milli seconds
    var duration = (consumerConfiguration.batchDurationSeconds * 1000).asInstanceOf[Int].asInstanceOf[Long];

    // We are going to set a timer
    val t = new java.util.Timer()

    // Run a full scan of the folder every X seconds
    val task = new java.util.TimerTask {
      def run() = {
        scanFolder()
      }
    }
    // Schedule
    t.schedule(task, duration, duration)


  }
}
