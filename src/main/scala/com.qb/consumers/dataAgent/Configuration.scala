package com.qb.consumers.dataAgent

import com.typesafe.config.Config

/**
  * Created by Stephen Borg on 02/10/2016.
  */
trait Configuration
{
  protected val config: Config

  // Get the element of properties within dataAgent
  private lazy val consumerPropsObject = config.getObject("dataAgentConsumer.properties")

  lazy val consumerConfig = ConsumerConfiguration(
    config.getInt("dataAgentConsumer.threads"),
    config.getString("dataAgentConsumer.dataDirectory"),
    config.getString("dataAgentConsumer.fileTypes"),
    config.getString("dataAgentConsumer.processedRepository"),
    config.getString("dataAgentConsumer.fileFormat"),
    config.getInt("dataAgentConsumer.batchDurationSeconds"),
    config.getInt("dataAgentConsumer.akkaProcessTimeout"),
    config.getString("dataAgentConsumer.serviceURL")
  )
}

// The class that represents all the configurations
case class ConsumerConfiguration(threads: Int,directory : String,fileTypes : String,processedRepository : String, fileFormat : String,batchDurationSeconds : Int, akkaProcessTimeout : Int, serviceURL : String)