package com.qb.testing

import akka.actor.{Props, ActorRef, ActorSystem}
import com.qb.consumers.dataAgent.{FileConsumer, Configuration}
import com.qb.consumers.helpers.actors.DataAgentManager
import com.qb.utilities.ConfigurationUtils

/**
  * Created by Stephen Borg on 03/10/2016.
  */
object BootTest {
  def main(args: Array[String]): Unit ={
    // Initialise the properties, and load them
    val config = new Configuration {
      override val config = ConfigurationUtils(args)
    }

    // Initialise akka system for our data agent
    implicit val system = ActorSystem("qb-dataAgent")

    val fileTypes = new Array[String](1)
    fileTypes(0) = "csv"

    val cacheManager: ActorRef = system.actorOf(Props(classOf[DataAgentManager], Map(),fileTypes), "qb-manager")
    val agentBooter = new FileConsumer(config.consumerConfig, cacheManager)
    agentBooter.run()
  }
}
