package com.qb.utilities

import java.io.File

import com.typesafe.config.{ConfigFactory, Config}
import org.slf4j.LoggerFactory

/**
  * Created by Stephen Borg on 02/10/2016.
  */
object ConfigurationUtils {
  val logger = LoggerFactory.getLogger(getClass)

  def apply(args: Array[String]): Config = {
    val defaultConfig = ConfigFactory.load("application.conf")
    val config = if (args.length > 0) {
      val configFile = new File(args(0))
      if (!configFile.exists())
      {
        logger.info("Could not find configuration file " + configFile)
        sys.exit(1)
      }
      ConfigFactory.parseFile(configFile).withFallback(defaultConfig)
    } else {
      defaultConfig
    }.resolve()

    config
  }
}
