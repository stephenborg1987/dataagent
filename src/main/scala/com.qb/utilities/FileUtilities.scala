package com.qb.utilities

import java.nio.file.Path
import java.util.Date

import com.qb.consumers.helpers.models.{JSONFileScan, CSVFileScan, FileScan}

/**
  * Created by Stephen Borg on 08/10/2016.
  */
class FileUtilities
{
    def getFile(FileName : String, DateProcessed : Date, Content : String, FileType : String, FilePath : Path): FileScan ={
      if(FileType.toLowerCase.equals("csv")){
          CSVFileScan(FileName,DateProcessed,Content,FileType,FilePath)
      }else if (FileType.toLowerCase.equals("json")){
        JSONFileScan(FileName,DateProcessed,Content,FileType,FilePath)
      }else{
        throw new IllegalArgumentException("FileType not supported!")
      }
    }
}


