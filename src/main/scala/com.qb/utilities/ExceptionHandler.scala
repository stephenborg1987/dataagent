package com.qb.utilities

import org.apache.log4j.Logger

/**
  * Created by Stephen Borg on 02/10/2016.
  */
object ExceptionHandler {

  def log(throwable: Throwable)(implicit logger: Logger) = logger.error("ExceptionHandler", throwable)

}
