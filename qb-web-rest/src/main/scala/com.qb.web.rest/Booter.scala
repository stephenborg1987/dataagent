package com.qb.web.rest

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer

import scala.concurrent.Future


/**
  * Created by Stephen Borg on 03/10/2016.
  */
object Booter {

  def printFile(obj: Object): Future[Done] = ???

  def main(args: Array[String]) {

    // needed to run the route
    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()

    // needed for the future flatMap/onComplete in the end
    implicit val executionContext = system.dispatcher

    val route: Route =
      post {
        path("pushFile") {
          entity(as[Object]) { file =>
            System.out.println(file)
            val displayed = printFile(file)
            onComplete(displayed) { done =>
              complete(null)
            }
          }
        }
      }


  }
}